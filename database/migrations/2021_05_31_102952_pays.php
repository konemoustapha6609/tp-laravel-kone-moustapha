<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Pays extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        schema::create('pays',function(Blueprint $table){
            $table->id();
            $table->string('libelle');
            $table->string('description');
            $table->string('code_indicatif');
            $table->string('continent');
            $table->integer('population');
            $table->string('capital');
            $table->string('monnaie');
            $table->integer('superficie');
            $table->boolean('etre-laique');
            $table->timestamps();
        });}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
